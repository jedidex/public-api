package com.holodex.publicapi.repository.astrographical;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.astrographical.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "region", path = "regions",
        excerptProjection = SWElement.Slim.class)
public interface RegionRepository extends Neo4jRepository<Region, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Region> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Region element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Region {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Region> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Region) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Region> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Region RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Region RETURN count(node) ")
    Page<Region> findByName(@Param("query") String query, Pageable p);
}