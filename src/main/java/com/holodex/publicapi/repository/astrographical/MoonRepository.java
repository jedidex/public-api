package com.holodex.publicapi.repository.astrographical;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.astrographical.Moon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "moon", path = "moons",
        excerptProjection = SWElement.Slim.class)
public interface MoonRepository extends Neo4jRepository<Moon, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Moon> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Moon element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Moon {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Moon> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Moon) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Moon> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Moon RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Moon RETURN count(node) ")
    Page<Moon> findByName(@Param("query") String query, Pageable p);
}