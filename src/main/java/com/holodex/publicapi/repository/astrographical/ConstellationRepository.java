package com.holodex.publicapi.repository.astrographical;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.astrographical.Constellation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "constellation", path = "constellations",
        excerptProjection = SWElement.Slim.class)
public interface ConstellationRepository extends Neo4jRepository<Constellation, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Constellation> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Constellation element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Constellation {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Constellation> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Constellation) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Constellation> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Constellation RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Constellation RETURN count(node) ")
    Page<Constellation> findByName(@Param("query") String query, Pageable p);
}