package com.holodex.publicapi.repository.astrographical;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.astrographical.Nebula;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "nebula", path = "nebulae",
        excerptProjection = SWElement.Slim.class)
public interface NebulaRepository extends Neo4jRepository<Nebula, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Nebula> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Nebula element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Nebula {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Nebula> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Nebula) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Nebula> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Nebula RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Nebula RETURN count(node) ")
    Page<Nebula> findByName(@Param("query") String query, Pageable p);
}