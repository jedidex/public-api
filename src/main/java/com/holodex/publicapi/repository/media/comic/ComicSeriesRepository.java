package com.holodex.publicapi.repository.media.comic;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.media.comics.ComicSeries;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "comic-series", path = "comic-series",
        excerptProjection = SWElement.Slim.class)
public interface ComicSeriesRepository extends Neo4jRepository<ComicSeries, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends ComicSeries> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(ComicSeries element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:ComicSeries {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<ComicSeries> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:ComicSeries) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<ComicSeries> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:ComicSeries RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:ComicSeries RETURN count(node) ")
    Page<ComicSeries> findByName(@Param("query") String query, Pageable p);
}