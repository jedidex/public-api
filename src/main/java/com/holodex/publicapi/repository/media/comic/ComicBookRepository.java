package com.holodex.publicapi.repository.media.comic;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.media.comics.ComicBook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "comic-book", path = "comic-books",
        excerptProjection = SWElement.Slim.class)
public interface ComicBookRepository extends Neo4jRepository<ComicBook, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends ComicBook> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(ComicBook element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:ComicBook {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<ComicBook> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:ComicBook) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<ComicBook> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:ComicBook RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:ComicBook RETURN count(node) ")
    Page<ComicBook> findByName(@Param("query") String query, Pageable p);
}