package com.holodex.publicapi.repository.media.book;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.media.books.BookSeries;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "book-series", path = "book-series",
        excerptProjection = SWElement.Slim.class)
public interface BookSeriesRepository extends Neo4jRepository<BookSeries, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends BookSeries> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(BookSeries element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:BookSeries {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<BookSeries> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:BookSeries) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<BookSeries> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:BookSeries RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:BookSeries RETURN count(node) ")
    Page<BookSeries> findByName(@Param("query") String query, Pageable p);
}