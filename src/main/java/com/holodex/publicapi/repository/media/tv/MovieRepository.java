package com.holodex.publicapi.repository.media.tv;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.media.tv.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "movie", path = "movies",
        excerptProjection = SWElement.Slim.class)
public interface MovieRepository extends Neo4jRepository<Movie, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Movie> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Movie element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Movie {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Movie> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Movie) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Movie> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Movie RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Movie RETURN count(node) ")
    Page<Movie> findByName(@Param("query") String query, Pageable p);
}