package com.holodex.publicapi.repository.media.tv;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.media.tv.TVEpisode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "tv-episode", path = "tv-episodes",
        excerptProjection = SWElement.Slim.class)
public interface TVEpisodeRepository extends Neo4jRepository<TVEpisode, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends TVEpisode> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(TVEpisode element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:TVEpisode {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<TVEpisode> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:TVEpisode) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<TVEpisode> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:TVEpisode RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:TVEpisode RETURN count(node) ")
    Page<TVEpisode> findByName(@Param("query") String query, Pageable p);
}