package com.holodex.publicapi.repository.media.book;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.media.books.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "book", path = "books",
        excerptProjection = SWElement.Slim.class)
public interface BookRepository extends Neo4jRepository<Book, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Book> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Book element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Book {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Book> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Book) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Book> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Book RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Book RETURN count(node) ")
    Page<Book> findByName(@Param("query") String query, Pageable p);
}