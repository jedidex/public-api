package com.holodex.publicapi.repository.media.tv;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.media.tv.TVSeason;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "tv-season", path = "tv-seasons",
        excerptProjection = SWElement.Slim.class)
public interface TVSeasonRepository extends Neo4jRepository<TVSeason, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends TVSeason> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(TVSeason element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:TVSeason {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<TVSeason> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:TVSeason) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<TVSeason> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:TVSeason RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:TVSeason RETURN count(node) ")
    Page<TVSeason> findByName(@Param("query") String query, Pageable p);
}