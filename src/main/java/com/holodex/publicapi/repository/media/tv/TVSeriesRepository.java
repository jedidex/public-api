package com.holodex.publicapi.repository.media.tv;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.media.tv.TVSeries;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "tv-series", path = "tv-series",
        excerptProjection = SWElement.Slim.class)
public interface TVSeriesRepository extends Neo4jRepository<TVSeries, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends TVSeries> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(TVSeries element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:TVSeries {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<TVSeries> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:TVSeries) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<TVSeries> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:TVSeries RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:TVSeries RETURN count(node) ")
    Page<TVSeries> findByName(@Param("query") String query, Pageable p);
}