package com.holodex.publicapi.repository.location;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.location.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "city", path = "cities",
        excerptProjection = SWElement.Slim.class)
public interface CityRepository extends Neo4jRepository<City, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends City> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(City element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:City {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<City> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:City) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<City> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:City RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:City RETURN count(node) ")
    Page<City> findByName(@Param("query") String query, Pageable p);
}