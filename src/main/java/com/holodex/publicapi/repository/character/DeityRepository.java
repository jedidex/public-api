package com.holodex.publicapi.repository.character;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.character.Deity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "deity", path = "deities",
        excerptProjection = SWElement.Slim.class)
public interface DeityRepository extends Neo4jRepository<Deity, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Deity> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Deity element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Deity {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Deity> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Deity) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Deity> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Deity RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Deity RETURN count(node) ")
    Page<Deity> findByName(@Param("query") String query, Pageable p);
}