package com.holodex.publicapi.repository.character;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.character.Character;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "character", path = "characters",
        excerptProjection = SWElement.Slim.class)
public interface CharacterRepository extends Neo4jRepository<Character, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Character> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Character element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH (n:Character {element_id:$id})-[r]-(x) RETURN n, collect(x), collect(r)")
    Optional<Character> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Character) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Character> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Character RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Character RETURN count(node) ")
    Page<Character> findByName(@Param("query") String query, Pageable p);


}
