package com.holodex.publicapi.repository.character;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.character.Droid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "droid", path = "droids",
        excerptProjection = SWElement.Slim.class)
public interface DroidRepository extends Neo4jRepository<Droid, Integer> {


    @Override
    @RestResource(exported = false)
    <S extends Droid> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Droid element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Droid {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Droid> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Droid) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Droid) RETURN count(n)")
    Page<Droid> findAll(Pageable pageable);


    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Droid RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Droid RETURN count(node) ")
    Page<Droid> findByName(@Param("query") String query, Pageable p);
}
