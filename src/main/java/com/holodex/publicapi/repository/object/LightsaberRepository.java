package com.holodex.publicapi.repository.object;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.object.Lightsaber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "lightsaber", path = "lightsabers",
        excerptProjection = SWElement.Slim.class)
public interface LightsaberRepository extends Neo4jRepository<Lightsaber, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Lightsaber> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Lightsaber element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Lightsaber {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Lightsaber> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Lightsaber) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Lightsaber> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Lightsaber RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Lightsaber RETURN count(node) ")
    Page<Lightsaber> findByName(@Param("query") String query, Pageable p);
}