package com.holodex.publicapi.repository.spacecraft;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.spacecraft.IndividualShip;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "individual-ship", path = "individual-ships",
        excerptProjection = SWElement.Slim.class)
public interface IndividualShipRepository extends Neo4jRepository<IndividualShip, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends IndividualShip> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(IndividualShip element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:IndividualShip {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<IndividualShip> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:IndividualShip) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<IndividualShip> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:IndividualShip RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:IndividualShip RETURN count(node) ")
    Page<IndividualShip> findByName(@Param("query") String query, Pageable p);
}