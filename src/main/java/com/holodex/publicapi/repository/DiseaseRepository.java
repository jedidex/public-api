package com.holodex.publicapi.repository;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.Disease;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "disease", path = "disease",
        excerptProjection = SWElement.Slim.class)
public interface DiseaseRepository extends Neo4jRepository<Disease, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends Disease> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(Disease element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:Disease {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<Disease> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:Disease) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<Disease> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:Disease RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:Disease RETURN count(node) ")
    Page<Disease> findByName(@Param("query") String query, Pageable p);
}