package com.holodex.publicapi.repository;

import com.holodex.publicapi.model.SWElement;
import com.holodex.publicapi.model.resource.DroidSeries;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "droid-series", path = "droid-series",
        excerptProjection = SWElement.Slim.class)
public interface DroidSeriesRepository extends Neo4jRepository<DroidSeries, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends DroidSeries> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(DroidSeries element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n:DroidSeries {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<DroidSeries> findById(Integer id);

    @Override
    @Query(value = "MATCH (n:DroidSeries) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n:Character) RETURN count(n)")
    Page<DroidSeries> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score where node:DroidSeries RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node where node:DroidSeries RETURN count(node) ")
    Page<DroidSeries> findByName(@Param("query") String query, Pageable p);
}