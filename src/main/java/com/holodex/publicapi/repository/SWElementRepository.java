package com.holodex.publicapi.repository;

import com.holodex.publicapi.model.SWElement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "element", path = "elements",
        itemResourceRel = "collectionResourceDescription")
public interface SWElementRepository extends Neo4jRepository<SWElement, Integer> {

    @Override
    @RestResource(exported = false)
    <S extends SWElement> S save(S element);

    @Override
    @RestResource(exported = false)
    void delete(SWElement element);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer id);

    @Override
    @Query("MATCH path=(n {element_id:$id})-[r]-(x) RETURN n, collect(nodes(path)), collect(relationships(path))")
    Optional<SWElement> findById(Integer id);

    @Override
    @Query(value = "MATCH (n) return n SKIP $skip LIMIT $limit",
            countQuery = "MATCH (n) RETURN count(n)")
    Page<SWElement> findAll(Pageable pageable);

    @RestResource(path = "name", rel = "searchByName")
    @Query(value = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score RETURN node, score SKIP $skip LIMIT $limit",
            countQuery = "CALL db.index.fulltext.queryNodes(\"name\", $query) YIELD node, score RETURN count(node) ")
    Page<SWElement> findByName(@Param("query") String query, Pageable p);


}
