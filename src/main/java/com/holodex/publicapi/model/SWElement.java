package com.holodex.publicapi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.rest.core.config.Projection;

@Getter
@Setter
@Node
public class SWElement {
    @Id
    @Property("element_id")
    private int elementId;

    private String name;
    private String image;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("search_score")
    private Double score;

    @Property("_wookiepedia_ref")
    private String wookiepediaRef;


    @Projection(name = "element", types = {SWElement.class})
    public interface Slim {
        String getName();

        int getElementId();

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty("search_score")
        Double getScore();

        String getWookiepediaRef();

        String getImage();

    }

}
