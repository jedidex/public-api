package com.holodex.publicapi.model.resource.media.tv;

import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("TVSeries")
public class TVSeries extends SWElement {

    private String format;
    private Double num_episodes;
    private Double num_seasons;
    private String runtime;
    private List<String> network;
    private String first_aired;
    private String last_aired;
    @Relationship(type = "CREATED_BY")
    private Set<SWElement> creators;

    @Relationship(type = "WRITTEN_BY")
    private Set<SWElement> writers;

    @Relationship(type = "DIRECTED_BY")
    private Set<SWElement> directors;

    @Relationship(type = "PRODUCED_BY")
    private Set<SWElement> executive_producers;

    @Relationship(type = "ACTED_BY")
    private Set<SWElement> starring;

    private String timeline;

    @Projection(name = "include_nested", types = {TVSeries.class})
    public interface CompleteProjection extends Slim {
        String getFormat();

        Double getNum_episodes();

        Double getNum_seasons();

        String getRuntime();

        List<String> getNetwork();

        String getFirst_aired();

        String getLast_aired();

        String getTimeline();
    }
}
