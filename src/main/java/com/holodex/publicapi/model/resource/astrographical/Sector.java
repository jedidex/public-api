package com.holodex.publicapi.model.resource.astrographical;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("Sector")
public class Sector extends SWElement {

    @Relationship(type = "PART_OF_REGION")
    private SWElement region;

    @Relationship(type = "HAS_SYSTEM")
    private Set<SWElement> systems;

    @Relationship(type = "HAS_PLANET")
    private Set<SWElement> planets;

    @Relationship(type = "HAS_MOON")
    private Set<SWElement> moons;

    @Relationship(type = "HAS_STATION")
    private Set<SWElement> stations;

    @Relationship(type = "HAS_ASTEROID")
    private Set<SWElement> asteroids;

    @Relationship(type = "HAS_COMET")
    private Set<SWElement> comets;

    @Relationship(type = "HAS_NEBULA")
    private Set<SWElement> nebulae;

    @Relationship(type = "HAS_ELEMENT")
    private Set<SWElement> other;

    @Relationship(type = "HAS_ROUTE")
    private Set<SWElement> routes;

    @Relationship(type = "HAS_SUB_SECTOR")
    private Set<SWElement> subsectors;

    @Relationship(type = "HAS_QUADRANT")
    private Set<SWElement> quadrants;

    @Relationship(type = "HAS_NATIVE_SPECIES")
    private Set<SWElement> native_species;

    @Relationship(type = "HAS_CAPITAL")
    private Set<SWElement> capital;

    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Sector.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getRegion();

    }
}
