package com.holodex.publicapi.model.resource.object;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("Lightsaber")
public class Lightsaber extends SWElement {

    private String model;
    private String type;
    @Relationship(type = "OF_CULTURE")
    private Set<SWElement> culture;

    @Relationship(type = "BUILT_BY")
    private SWElement creator;

    private String created;
    private String destroyed;
    private String discovered;
    @Relationship(type = "OWNED_BY")
    private Set<SWElement> owners;

    private String hilt_model;
    private String hilt_shape;
    private Double hilt_length;
    private String hilt_material;
    @Relationship(type = "USES_CRYSTAL")
    private Set<SWElement> crystal;

    private String blade_type;
    private String color;
    private String blade_length;
    private String mods;
    private String weight;
    private String protection;
    private String capacity;
    private String range;
    private String design;
    private String markings;
    private String purpose;
    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Lightsaber.class})
    public interface CompleteProjection extends Slim {
        String getModel();

        String getType();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getCreator();

        String getCreated();

        String getDestroyed();

        String getDiscovered();

        String getHilt_model();

        String getHilt_shape();

        Double getHilt_length();

        String getHilt_material();

        String getBlade_type();

        String getColor();

        String getBlade_length();

        String getMods();

        String getWeight();

        String getProtection();

        String getCapacity();

        String getRange();

        String getDesign();

        String getMarkings();

        String getPurpose();
    }
}
