package com.holodex.publicapi.model.resource.location;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("City")
public class City extends SWElement {

    private String constructed;
    private String destroyed;
    private String rebuilt;
    @Relationship(type = "BUILT_BY")
    private Set<SWElement> builder;

    @Relationship(type = "HAS_MAYOR")
    private Set<SWElement> mayor;

    @Relationship(type = "ON_PLANET")
    private SWElement planet;

    @Relationship(type = "ON_MOON")
    private SWElement moon;

    private String continent;
    @Relationship(type = "LOCATED_ON")
    private Set<SWElement> location;

    private String climate;
    @Relationship(type = "HAS_POI")
    private Set<SWElement> interest;

    private String population;
    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {City.class})
    public interface CompleteProjection extends Slim {
        String getConstructed();

        String getDestroyed();

        String getRebuilt();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getPlanet();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getMoon();

        String getContinent();

        String getClimate();

        String getPopulation();
    }
}
