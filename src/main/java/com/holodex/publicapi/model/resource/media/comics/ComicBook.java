package com.holodex.publicapi.model.resource.media.comics;

import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("ComicBook")
public class ComicBook extends SWElement {

    @Relationship(type = "WRITTEN_BY")
    private Set<SWElement> writer;

    @Relationship(type = "DRAWN_BY")
    private Set<SWElement> penciller;

    @Relationship(type = "INKED_BY")
    private Set<SWElement> inker;

    @Relationship(type = "LETTERED_BY")
    private Set<SWElement> letterer;

    @Relationship(type = "COLOR_BY")
    private Set<SWElement> colorist;

    @Relationship(type = "COVER_BY")
    private Set<SWElement> cover_artist;

    @Relationship(type = "EDITED_BY")
    private Set<SWElement> editor;

    @Relationship(type = "PUBLISHED_BY")
    private Set<SWElement> publisher;

    private String publication_date;
    private Double pages;
    private String upc;
    private Double issue;
    private String timeline;
    @Relationship(type = "PART_OF")
    private Set<SWElement> series;

    @Relationship(type = "PRECEEDED_BY")
    private Set<SWElement> preceded_by;

    @Relationship(type = "FOLLOWED_BY")
    private Set<SWElement> followed_by;

    @Projection(name = "include_nested", types = {ComicBook.class})
    public interface CompleteProjection extends Slim {
        String getPublication_date();

        Double getPages();

        String getUpc();

        Double getIssue();

        String getTimeline();
    }
}
