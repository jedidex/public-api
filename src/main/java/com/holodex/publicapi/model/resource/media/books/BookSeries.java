package com.holodex.publicapi.model.resource.media.books;

import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("BookSeries")
public class BookSeries extends SWElement {

    @Relationship(type = "WRITTEN_BY")
    private Set<SWElement> author;

    @Relationship(type = "COVER_BY")
    private Set<SWElement> cover_artist;

    @Relationship(type = "ILLUSTRATIONS_BY")
    private Set<SWElement> illustrator;

    @Relationship(type = "EDITED_BY")
    private Set<SWElement> editor;

    @Relationship(type = "PUBLISHED_BY")
    private Set<SWElement> publisher;

    private String last_date;
    private String first_date;
    private List<String> media_type;
    private Double pages;
    private String isbn;
    private Double num_books;
    private String timeline;
    @Relationship(type = "PRECEEDED_BY")
    private Set<SWElement> preceded_by;

    @Relationship(type = "FOLLOWED_BY")
    private Set<SWElement> followed_by;

    @Projection(name = "include_nested", types = {BookSeries.class})
    public interface CompleteProjection extends Slim {
        String getLast_date();

        String getFirst_date();

        List<String> getMedia_type();

        Double getPages();

        String getIsbn();

        Double getNum_books();

        String getTimeline();
    }
}
