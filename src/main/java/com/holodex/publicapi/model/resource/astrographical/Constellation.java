package com.holodex.publicapi.model.resource.astrographical;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("Constellation")
public class Constellation extends SWElement {

    @Relationship(type = "PART_OF_REGION")
    private SWElement region;

    @Relationship(type = "PART_OF_SECTOR")
    private SWElement sector;

    @Relationship(type = "HAS_SYSTEM")
    private Set<SWElement> systems;

    private String coordinates;
    private String xyz;
    @Relationship(type = "HAS_SUN")
    private Set<SWElement> suns;

    @Relationship(type = "HAS_ORBIT")
    private Set<SWElement> orbits;

    @Relationship(type = "HAS_STATION")
    private Set<SWElement> stations;

    @Relationship(type = "HAS_ASTEROID")
    private Set<SWElement> asteroids;

    @Relationship(type = "HAS_COMET")
    private Set<SWElement> comets;

    @Relationship(type = "HAS_NEBULA")
    private Set<SWElement> nebulae;

    @Relationship(type = "HAS_ELEMENT")
    private Set<SWElement> other;

    @Relationship(type = "HAS_ROUTE")
    private Set<SWElement> routes;

    @Relationship(type = "HAS_QUADRANT")
    private Set<SWElement> quadrants;

    @Relationship(type = "HAS_NATIVE_SPECIES")
    private Set<SWElement> native_species;

    @Relationship(type = "HAS_SPECIES")
    private Set<SWElement> other_species;

    @Relationship(type = "HAS_LANGUAGE")
    private Set<SWElement> language;

    private List<String> population;
    private List<String> imports;
    private List<String> exports;
    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Constellation.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getRegion();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getSector();

        String getCoordinates();

        String getXyz();

        List<String> getPopulation();

        List<String> getImports();

        List<String> getExports();
    }
}
