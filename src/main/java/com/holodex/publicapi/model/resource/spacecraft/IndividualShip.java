package com.holodex.publicapi.model.resource.spacecraft;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("IndividualShip")
public class IndividualShip extends SWElement {

    @Relationship(type = "MANUFACTURED_BY")
    private SWElement manufacturer;

    @Relationship(type = "OF_SERIES")
    private SWElement line;

    @Relationship(type = "OF_MODEL")
    private SWElement model;

    @Relationship(type = "OF_CLASS")
    private SWElement className;

    private Double cost;
    @Relationship(type = "MODIFIED_BY")
    private Set<SWElement> modifier;

    private List<String> sysmods;
    private Double length;
    private Double width;
    private Double height;
    private Double mass;
    private String max_accel;
    private String mglt;
    private Double max_speed;
    private String maneuverability;
    private String engine;
    private List<String> hyperdrive_rating;
    private String hdrange;
    @Relationship(type = "USES_HYPERDRIVE")
    private Set<SWElement> hdsystem;

    private String poweroutput;
    private List<String> power;
    private List<String> shield_gen;
    private List<String> hull;
    private List<String> sensor;
    private List<String> target;
    private List<String> navigation;
    private List<String> avionics;
    private List<String> maincomp;
    private List<String> countermeasures;
    private List<String> armament;
    @Relationship(type = "HAS_COMPLEMENT")
    private Set<SWElement> complement;

    private String bays;
    @Relationship(type = "HAS_COMPLEMENT")
    private Set<SWElement> escapepods;

    private String crew;
    private String min_crew;
    private String passengers;
    private String capacity;
    private String cargohandling;
    private String consumables;
    private String lifesupport;
    private String communications;
    private List<String> othersystems;
    private String availability;
    private List<String> role;
    private String commission;
    private String destroyed;
    private String retired;
    @Relationship(type = "TAKE_PART_TO")
    private Set<SWElement> battles;

    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "PART_OF_NAVY")
    private Set<SWElement> navy;

    @Relationship(type = "PART_OF_FLEET")
    private Set<SWElement> fleet;

    @Relationship(type = "OWNED_BY")
    private Set<SWElement> owners;

    @Relationship(type = "CAPITANINED_BY")
    private Set<SWElement> captains;

    @Relationship(type = "HAS_BEEN_CREW")
    private Set<SWElement> namedcrew;

    private String registry;
    private List<String> aliases;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {IndividualShip.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getManufacturer();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getLine();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getModel();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getClassName();

        Double getCost();

        List<String> getSysmods();

        Double getLength();

        Double getWidth();

        Double getHeight();

        Double getMass();

        String getMax_accel();

        String getMglt();

        Double getMax_speed();

        String getManeuverability();

        String getEngine();

        List<String> getHyperdrive_rating();

        String getHdrange();

        String getPoweroutput();

        List<String> getPower();

        List<String> getShield_gen();

        List<String> getHull();

        List<String> getSensor();

        List<String> getTarget();

        List<String> getNavigation();

        List<String> getAvionics();

        List<String> getMaincomp();

        List<String> getCountermeasures();

        List<String> getArmament();

        String getBays();

        String getCrew();

        String getMin_crew();

        String getPassengers();

        String getCapacity();

        String getCargohandling();

        String getConsumables();

        String getLifesupport();

        String getCommunications();

        List<String> getOthersystems();

        String getAvailability();

        List<String> getRole();

        String getCommission();

        String getDestroyed();

        String getRetired();

        String getRegistry();

        List<String> getAliases();
    }
}
