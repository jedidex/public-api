package com.holodex.publicapi.model.resource.media.tv;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("TVSeason")
public class TVSeason extends SWElement {

    @Relationship(type = "PART_OF")
    private SWElement series;

    private String format;
    private Double num_episodes;
    private String runtime;
    private List<String> network;
    private String first_aired;
    private String last_aired;
    @Relationship(type = "CREATED_BY")
    private Set<SWElement> creators;

    @Relationship(type = "WRITTEN_BY")
    private Set<SWElement> writers;

    @Relationship(type = "DIRECTED_BY")
    private Set<SWElement> directors;

    @Relationship(type = "PRODUCED_BY")
    private Set<SWElement> executive_producers;

    @Relationship(type = "ACTED_BY")
    private Set<SWElement> starring;

    private String timeline;
    @Relationship(type = "PRECEEDED_BY")
    private Set<SWElement> preceded_by;

    @Relationship(type = "FOLLOWED_BY")
    private Set<SWElement> followed_by;

    @Projection(name = "include_nested", types = {TVSeason.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getSeries();

        String getFormat();

        Double getNum_episodes();

        String getRuntime();

        List<String> getNetwork();

        String getFirst_aired();

        String getLast_aired();

        String getTimeline();
    }
}
