package com.holodex.publicapi.model.resource.astrographical;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("Nebula")
public class Nebula extends SWElement {

    @Relationship(type = "PART_OF_REGION")
    private SWElement region;

    @Relationship(type = "PART_OF_SECTOR")
    private SWElement sector;

    private String coordinates;
    private String xyz;
    @Relationship(type = "HAS_SUN")
    private Set<SWElement> suns;

    @Relationship(type = "HAS_STATION")
    private Set<SWElement> stations;

    @Relationship(type = "HAS_ASTEROID")
    private Set<SWElement> asteroids;

    @Relationship(type = "HAS_COMET")
    private Set<SWElement> comets;

    @Relationship(type = "HAS_ELEMENT")
    private Set<SWElement> other;

    @Relationship(type = "HAS_ROUTE")
    private Set<SWElement> routes;

    @Relationship(type = "HAS_NATIVE_SPECIES")
    private Set<SWElement> native_species;

    @Relationship(type = "HAS_SPECIES")
    private Set<SWElement> other_species;

    @Relationship(type = "HAS_LANGUAGE")
    private Set<SWElement> language;

    private List<String> population;
    private List<String> imports;
    private List<String> exports;
    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    private String distance;
    private Double size;
    private String formed;
    @Relationship(type = "HAS_SYSTEM")
    private Set<SWElement> system;

    @Relationship(type = "HAS_PLANET")
    private Set<SWElement> planets;

    private List<String> distinctions;
    @Relationship(type = "HAS_GOVERNMENT")
    private Set<SWElement> government;

    @Relationship(type = "HAS_POI")
    private Set<SWElement> interest;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Nebula.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getRegion();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getSector();

        String getCoordinates();

        String getXyz();

        List<String> getPopulation();

        List<String> getImports();

        List<String> getExports();

        String getDistance();

        Double getSize();

        String getFormed();

        List<String> getDistinctions();
    }
}
