package com.holodex.publicapi.model.resource.media.tv;

import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("Movie")
public class Movie extends SWElement {

    @Relationship(type = "DIRECTED_BY")
    private Set<SWElement> director;

    @Relationship(type = "PRODUCED_BY")
    private Set<SWElement> producer;

    @Relationship(type = "WRITTEN_BY")
    private Set<SWElement> writer;

    @Relationship(type = "STARRING")
    private Set<SWElement> starring;

    @Relationship(type = "MUSIC_BY")
    private Set<SWElement> music;

    private String release_date;
    private String runtime;
    private String budget;
    private String language;
    private String timeline;
    @Relationship(type = "PART_OF")
    private Set<SWElement> series;

    @Relationship(type = "PRECEEDED_BY")
    private Set<SWElement> preceded_by;

    @Relationship(type = "FOLLOWED_BY")
    private Set<SWElement> followed_by;

    @Projection(name = "include_nested", types = {Movie.class})
    public interface CompleteProjection extends Slim {
        String getRelease_date();

        String getRuntime();

        String getBudget();

        String getLanguage();

        String getTimeline();
    }
}
