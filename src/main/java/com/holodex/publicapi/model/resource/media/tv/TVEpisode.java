package com.holodex.publicapi.model.resource.media.tv;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("TVEpisode")
public class TVEpisode extends SWElement {

    @Relationship(type = "PART_OF")
    private SWElement series;

    @Relationship(type = "PART_OF_SEASON")
    private SWElement season;

    private Double episode;
    private String production;
    private String air_date;
    private String runtime;
    @Relationship(type = "DIRECTED_BY")
    private Set<SWElement> director;

    @Relationship(type = "WRITTEN_BY")
    private Set<SWElement> writer;

    @Relationship(type = "ACTED_BY")
    private Set<SWElement> guests;

    private String timeline;
    @Relationship(type = "PRECEEDED_BY")
    private Set<SWElement> preceded_by;

    @Relationship(type = "FOLLOWED_BY")
    private Set<SWElement> followed_by;

    @Projection(name = "include_nested", types = {TVEpisode.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getSeries();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getSeason();

        Double getEpisode();

        String getProduction();

        String getAir_date();

        String getRuntime();

        String getTimeline();
    }
}
