package com.holodex.publicapi.model.resource.media.books;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("Book")
public class Book extends SWElement {

    @Relationship(type = "WRITTEN_BY")
    private Set<SWElement> author;

    @Relationship(type = "COVER_BY")
    private Set<SWElement> cover_artist;

    @Relationship(type = "ILLUSTRATIONS_BY")
    private Set<SWElement> illustrator;

    @Relationship(type = "EDITED_BY")
    private Set<SWElement> editor;

    @Relationship(type = "PUBLISHED_BY")
    private Set<SWElement> publisher;

    private String release_date;
    private String media_type;
    private Double pages;
    private String isbn;
    private String timeline;
    @Relationship(type = "PART_OF")
    private SWElement series;

    @Relationship(type = "PRECEEDED_BY")
    private Set<SWElement> preceded_by;

    @Relationship(type = "FOLLOWED_BY")
    private Set<SWElement> followed_by;

    @Projection(name = "include_nested", types = {Book.class})
    public interface CompleteProjection extends Slim {
        String getRelease_date();

        String getMedia_type();

        Double getPages();

        String getIsbn();

        String getTimeline();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getSeries();

    }
}
