package com.holodex.publicapi.model.resource.astrographical;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("Moon")
public class Moon extends SWElement {

    @Relationship(type = "PART_OF_REGION")
    private SWElement region;

    @Relationship(type = "PART_OF_SECTOR")
    private SWElement sector;

    @Relationship(type = "PART_OF_SYSTEM")
    private SWElement system;

    @Relationship(type = "HAS_MOON")
    private Set<SWElement> moons;

    private String coordinates;
    @Relationship(type = "HAS_ROUTE")
    private Set<SWElement> routes;

    private String lengthday;
    private String lengthyear;
    private String moon_class;
    private Double diameter;
    private String atmosphere;
    private String climate;
    private String gravity;
    private List<String> terrain;
    private List<String> water;
    @Relationship(type = "HAS_POI")
    private Set<SWElement> interest;

    @Relationship(type = "HAS_FLORA")
    private Set<SWElement> flora;

    @Relationship(type = "HAS_FAUNA")
    private Set<SWElement> fauna;

    @Relationship(type = "HAS_NATIVE_SPECIES")
    private Set<SWElement> native_species;

    @Relationship(type = "HAS_SPECIES")
    private Set<SWElement> other_species;

    @Relationship(type = "HAS_LANGUAGE")
    private Set<SWElement> language;

    @Relationship(type = "HAS_GOVERNMENT")
    private Set<SWElement> government;

    private List<String> population;
    private List<String> demonym;
    @Relationship(type = "HAS_CITY")
    private Set<SWElement> cities;

    private List<String> imports;
    private List<String> exports;
    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "OF_PLANET")
    private SWElement planet;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Moon.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getRegion();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getSector();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getSystem();

        String getCoordinates();

        String getLengthday();

        String getLengthyear();

        String getMoon_class();

        Double getDiameter();

        String getAtmosphere();

        String getClimate();

        String getGravity();

        List<String> getTerrain();

        List<String> getWater();

        List<String> getPopulation();

        List<String> getDemonym();

        List<String> getImports();

        List<String> getExports();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getPlanet();

    }
}
