package com.holodex.publicapi.model.resource.location;

import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("Location")
public class Location extends SWElement {

    private String created;
    private String destroyed;
    @Relationship(type = "LOCATED_ON")
    private Set<SWElement> location;

    private String terrain;
    @Relationship(type = "HAS_FAUNA")
    private Set<SWElement> fauna;

    @Relationship(type = "HAS_FLORA")
    private Set<SWElement> flora;

    @Relationship(type = "HAS_POI")
    private Set<SWElement> interest;

    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Location.class})
    public interface CompleteProjection extends Slim {
        String getCreated();

        String getDestroyed();

        String getTerrain();
    }
}
