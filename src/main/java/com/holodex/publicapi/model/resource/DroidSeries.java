package com.holodex.publicapi.model.resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("DroidSeries")
public class DroidSeries extends SWElement {

    @Relationship(type = "BUILT_IN")
    private SWElement homeworld;

    @Relationship(type = "BUILT_BY")
    private Set<SWElement> creator;

    @Relationship(type = "MANUFACTURED_BY")
    private Set<SWElement> manufacturer;

    @Relationship(type = "OF_LINE")
    private Set<SWElement> line;

    @Relationship(type = "OF_SERIES")
    private Set<SWElement> model;

    @Property("class")
    private String className;
    private Double cost;
    private Double length;
    private Double width;
    private Double height;
    private Double mass;
    private List<String> sensor;
    private List<String> plating;
    private List<String> equipment;
    private String gender;
    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "DESIGNED_BY")
    private Set<SWElement> designer;

    @Relationship(type = "OF_DEGREE")
    private Set<SWElement> degree;

    @Relationship(type = "ARMED_WITH")
    private Set<SWElement> armament;

    private String first_made;
    private String retired;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {DroidSeries.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getHomeworld();

        String getClassName();

        Double getCost();

        Double getLength();

        Double getWidth();

        Double getHeight();

        Double getMass();

        List<String> getSensor();

        List<String> getPlating();

        List<String> getEquipment();

        String getGender();

        String getFirst_made();

        String getRetired();
    }
}
