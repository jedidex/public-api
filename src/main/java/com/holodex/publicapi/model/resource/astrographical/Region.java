package com.holodex.publicapi.model.resource.astrographical;

import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("Region")
public class Region extends SWElement {

    @Relationship(type = "HAS_SECTOR")
    private Set<SWElement> sectors;

    @Relationship(type = "HAS_SYSTEM")
    private Set<SWElement> systems;

    @Relationship(type = "HAS_PLANET")
    private Set<SWElement> planets;

    @Relationship(type = "HAS_ELEMENT")
    private Set<SWElement> other;

    @Relationship(type = "HAS_ROUTE")
    private Set<SWElement> routes;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Region.class})
    public interface CompleteProjection extends Slim {
    }
}
