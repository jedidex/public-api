package com.holodex.publicapi.model.resource.character;

import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("Deity")
public class Deity extends SWElement {

    @Relationship(type = "OF_RELIGION")
    private Set<SWElement> religion;

    @Relationship(type = "PANTHON_ON")
    private Set<SWElement> pantheon;

    @Relationship(type = "OF_WORSHIP")
    private Set<SWElement> worship;

    @Relationship(type = "OWNS")
    private Set<SWElement> artifacts;

    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "OF_AREA")
    private Set<SWElement> area;

    private String type;
    private String powers;
    private String form;
    private String gender;
    private String first_apparence;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Deity.class})
    public interface CompleteProjection extends Slim {
        String getType();

        String getPowers();

        String getForm();

        String getGender();

        String getFirst_apparence();
    }
}
