package com.holodex.publicapi.model.resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("Disease")
public class Disease extends SWElement {

    @Relationship(type = "CREATED_BY")
    private SWElement created_by;

    private String date_engineered;
    private List<String> number_infected;
    private List<String> number_killed;
    @Relationship(type = "AFFECT_SPECIES")
    private Set<SWElement> susceptible_species;

    private List<String> transmission_type;
    private String incubation_period;
    private List<String> symptoms;
    private List<String> treatments;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Disease.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getCreated_by();

        String getDate_engineered();

        List<String> getNumber_infected();

        List<String> getNumber_killed();

        List<String> getTransmission_type();

        String getIncubation_period();

        List<String> getSymptoms();

        List<String> getTreatments();
    }
}
