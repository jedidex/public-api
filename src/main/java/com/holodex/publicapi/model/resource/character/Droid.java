package com.holodex.publicapi.model.resource.character;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("Droid")
public class Droid extends SWElement {

    @Relationship(type = "BUILT_IN")
    private SWElement homeworld;

    private String birth;
    @Relationship(type = "ASSEMBLED_IN")
    private SWElement birth_location;

    private String death;
    @Relationship(type = "DISMANTLED_IN")
    private SWElement death_location;

    @Relationship(type = "BUILT_BY")
    private SWElement creator;

    @Relationship(type = "MANUFACTURED_BY")
    private Set<SWElement> manufacturer;

    @Relationship(type = "OF_LINE")
    private SWElement line;

    @Relationship(type = "OF_SERIES")
    private SWElement model;

    private String droid_class;
    private Double cost;
    private Double length;
    private Double width;
    private Double height;
    private Double mass;
    private List<String> sensor;
    private List<String> plating;
    private List<String> equipment;
    private String gender;
    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Droid.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getHomeworld();

        String getBirth();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getBirth_location();

        String getDeath();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getDeath_location();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getCreator();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getLine();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getModel();

        String getDroid_class();

        Double getCost();

        Double getLength();

        Double getWidth();

        Double getHeight();

        Double getMass();

        List<String> getSensor();

        List<String> getPlating();

        List<String> getEquipment();

        String getGender();
    }
}
