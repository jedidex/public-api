package com.holodex.publicapi.model.resource.character;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Getter
@Setter
@Node("Character")
public class Character extends SWElement {

    @Relationship(type = "FROM_WORLD")
    private SWElement homeworld;

    private String birth;
    @Relationship(type = "BORN_IN")
    private SWElement birth_location;

    private String death;
    @Relationship(type = "DEATH_IN")
    private SWElement death_location;

    @Relationship(type = "OF_SPIECES")
    private SWElement species;

    private String gender;
    private Double height;
    private Double mass;
    private String hair;
    private String eyes;
    private String skin;
    private String cyber;
    @Relationship(type = "AFFILIATED_TO")
    private Set<SWElement> affiliation;

    @Relationship(type = "APPRENTICE_OF")
    private Set<SWElement> masters;

    @Relationship(type = "MASTER_OF")
    private Set<SWElement> apprentices;

    @Relationship(type = "APPEARS_IN")
    private Set<SWElement> appears_in;

    @Projection(name = "include_nested", types = {Character.class})
    public interface CompleteProjection extends Slim {
        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getHomeworld();

        String getBirth();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getBirth_location();

        String getDeath();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getDeath_location();

        @JsonSerialize(as = SWElement.class)
        @JsonInclude(JsonInclude.Include.ALWAYS)
        SWElement getSpecies();

        String getGender();

        Double getHeight();

        Double getMass();

        String getHair();

        String getEyes();

        String getSkin();

        String getCyber();
    }
}
