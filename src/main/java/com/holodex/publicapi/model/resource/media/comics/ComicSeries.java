package com.holodex.publicapi.model.resource.media.comics;

import com.holodex.publicapi.model.SWElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Node("ComicSeries")
public class ComicSeries extends SWElement {

    @Relationship(type = "WRITTEN_BY")
    private Set<SWElement> writer;

    @Relationship(type = "DRAWN_BY")
    private Set<SWElement> penciller;

    @Relationship(type = "INKED_BY")
    private Set<SWElement> inker;

    @Relationship(type = "LETTERED_BY")
    private Set<SWElement> letterer;

    @Relationship(type = "COLOR_BY")
    private Set<SWElement> colorist;

    @Relationship(type = "COVER_BY")
    private Set<SWElement> cover_artist;

    @Relationship(type = "EDITED_BY")
    private Set<SWElement> editor;

    @Relationship(type = "PUBLISHED_BY")
    private Set<SWElement> publisher;

    private String start_date;
    private String end_date;
    private String schedule;
    private List<String> format;
    private Double issues;
    private String timeline;

    @Projection(name = "include_nested", types = {ComicSeries.class})
    public interface CompleteProjection extends Slim {
        String getStart_date();

        String getEnd_date();

        String getSchedule();

        List<String> getFormat();

        Double getIssues();

        String getTimeline();
    }
}
