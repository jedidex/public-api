package com.holodex.publicapi.config;

import io.swagger.v3.core.filter.SpecFilter;
import io.swagger.v3.oas.models.OpenAPI;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.stereotype.Component;

@Component
public class OpenApiTagsCustomizer extends SpecFilter implements OpenApiCustomiser {

    @Override
    public void customise(OpenAPI openApi) {
        // rename the operation tags merging for entity, thus obtaining a cleaner OpenAPI interface
        openApi.getPaths().values().stream().flatMap(pathItem -> pathItem.readOperations().stream())
                .forEach(operation -> {
                    String tagName = operation.getTags().get(0);
                    // rename the entity-controller and search-controller tags
                    if (tagName.endsWith("entity-controller") || tagName.endsWith("search-controller")) {
                        String myTagValue = tagName.substring(0, tagName.length() - 18);
                        // setting the first character capitalized
                        myTagValue = myTagValue.substring(0, 1).toUpperCase() + myTagValue.substring(1);
                        operation.getTags().set(0, myTagValue);
                    }
                    // rename the property-reference-controller
                    else if (tagName.endsWith("property-reference-controller")) {
                        String myTagValue = tagName.substring(0, tagName.length() - 30);
                        // setting the first character capitalized
                        myTagValue = myTagValue.substring(0, 1).toUpperCase() + myTagValue.substring(1);
                        operation.getTags().set(0, myTagValue);
                    }
                });
        removeBrokenReferenceDefinitions(openApi);
    }

}