package com.holodex.publicapi.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {

    private final OpenApiTagsCustomizer customizer;

    @Autowired
    public OpenAPIConfig(OpenApiTagsCustomizer customizer) {
        this.customizer = customizer;
    }

    @Bean
    public OpenAPI springOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("Jedidex API")
                        .description("Jedidex API Open API documentation")
                        .version("v1.0-SNAPSHOT")
                        .license(new License().name("Apache 2.0").url("https://www.apache.org/licenses/LICENSE-2.0")))
                .externalDocs(new ExternalDocumentation()
                        .description("Documentation")
                        .url("https://jedidex.com/docs/"));
    }

    @Bean
    public GroupedOpenApi hideApis() {
        // filtering out all the unnecessary endpoints
        return GroupedOpenApi.builder().group("default")
                .pathsToExclude("/**/search/findAll", "/**/search/findById", "/v1/profile/**", "/v1/profile", "/*/*/*/*/*")
                .pathsToMatch("/v1/**")
                .addOpenApiCustomiser(customizer)
                .build();
    }

    @Bean
    public GroupedOpenApi full() {
        // returning all the endpoints
        return GroupedOpenApi.builder().group("full set")
                .pathsToMatch("/**")
                .build();
    }


}