FROM openjdk:17-alpine
COPY /target/*.jar app.jar

RUN apk add --no-cache tzdata
ENV TZ Europe/Zurich

EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]